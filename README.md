Sonar Code Challenge
====================

## Requeriments

* Create a public Git repository on the platform of your choice (Github, Bitbucket, etc).

* Read about Bitso’s public REST API and WebSocket API. We will use the BTCMXN order book for all parts of this exercise.

* Follow the instructions detailed on the WebSocket API’s General page to maintain real-time orderbook state through coordination between REST and websocket. The only channel you should connect to is the ‘diff-orders’ channel. Do not connect to any others for any other reason. Pay special attention to this excerpt of the relevant instructions:

In theory, you can get a copy of the full order book via REST once, and keep it up to date by using the diff-orders channel with the following algorithm:

1. Subscribe to the diff-orders channel.
1. Queue any message that come in to this channel.
1.* Get the full orderbook from the REST orderbook endpoint.
1. Playback the queued message, discarding the ones with sequence number below or equal to the one from the REST orderbook.
1. Apply the next queued messages to your local order book data structure.
1. Apply real-time messages to your local orderbook as they come in through the stream.


* Display the **X** best bids and **X** best asks in real-time on a graphical UI. The number **X** is an integer and should be a runtime configuration.

* Use the REST API (not the websocket) to poll for recent trades at some regular interval.

* Display the **X** most recent trades on the same UI.
Write a contrarian trading strategy that does not actually execute trades, but will update the UI when it would have traded if this were in a live, production trading strategy. This strategy will work by counting the **M** consecutive upticks and **N** consecutive downticks. A trade that executes at a price that is the same as the price of the trade that executed immediately preceding it is known as a “zero tick”. An uptick is when a trade executes at a higher price than the most recent non-zero-tick trade before it. A downtick is when a trade executes at a lower price than the most recent non-zero-tick trade before it.  After **M** consecutive upticks, the algorithm should sell 1 BTC at the price of the most recent uptick. After **N** consecutive downticks, it should buy 1 BTC at the price of the most recent downtick. **M** and **N** should also be runtime configurations. For example:


	a. UP -> UP -> UP = 3 upticks
	b. UP -> DOWN -> UP = 1 uptick
	c. DOWN -> ZERO -> DOWN -> DOWN  = 3 downticks
	d. DOWN -> UP -> DOWN -> DOWN  = 2 downticks


* Instead of actually trading, the algorithm only needs to add the trade it would have wanted to execute to the list of trades that is displayed on the UI. There should be something on the UI that helps differentiate real trades from imaginary trades executed by the trading algorithm.



## Application

This is a JavaFX desktop application run by command line built essentially with Java 8 and Maven. 

**Overview:**

The application contains one pane where three different graphs are printed:

* Buys/Bids (green) shows _in real time_ the X best bids. This data comes from web socket

* Sells/Asks (red) shows _in real time_ the X best asks. This data comes from web socket

* Trades (blue) shows _on demand_ the X best trades. This data comes from rest api call


## How to build and run the application

**Building:**
1. Clone or download the code from [git repo](https://bitbucket.org/Scannerwebs2016/sonartrading)

2. _cd_ to the folder level _pom.xml_ file is

3. Execute **mvn jfx:jar** to run the tests and create the executable jar. You can add _-DskipTests=true_ to escaping the tests.


**Running:**

1. _cd_ to _target/jfx/app/_

2. Execute **java -jar com.sonar.trading-0.0.1-jfx.jar**



## How to operate

**Overview:**
The pane is divided into two sections:

a) On the left, the graph section:


* Y-axis showing Rate(Bids/Asks)/Price(Trades) 
* X-axis the number of Bids/Asks/Trades to show.
* Legend click-able to disable/enable the corresponding graph


b) On the right, the details box and input section:


* Order details box which is populated with order data when an order dot is clicked over 
* Trade details box which is populated with trade data when a trade dot is clicked over 
* Bids/Asks input to update the number of Bids/Asks/Trades shown in the graph after clicking _Refresh_ button. Minimum value allowed is 1 and maximum is 250.
* Strategy input to update the up and down ticks to apply for the trades after clicking _Update_ button.

![Application demo overview screenshot](screenshots/overview.JPG)


**Showing orders and trades details:**

Selecting buy orders

![Application demo overview screenshot](screenshots/buySelectedTradesDisabled.JPG)



Selecting buy orders

![Application demo overview screenshot](screenshots/sellSelectedTradesDisabled.JPG)



Selecting trades not marked by the strategy

![Application demo overview screenshot](screenshots/tradeNoStrategySelected.JPG)


**Strategy applied to trades:**


![Application demo overview screenshot](screenshots/trades.JPG)


## Author
[Jorge Marin Guerrero](https://www.linkedin.com/in/jorge-mar%C3%ADn-guerrero-806b3851/)





