package com.sonar.trading;

import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sonar.trading.controller.SonarController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


/**
 * JavaFX application to show bids/asks/trades
 *  
 * @author Jorge Marin Guerrero
 *
 */
public class SonarApplication extends Application{

	private static final Logger logger = LoggerFactory.getLogger(SonarApplication.class);
	
	private SonarController controller;

	@Override
	public void start(Stage stage) {
		stage.setTitle("Sonar Trading");        
		
		try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(SonarApplication.class.getResource("/sonar.fxml"));
            AnchorPane pane = loader.load();
            
            // Getting reference to controller to stop it at close method
            controller = loader.getController();
                     
            Scene scene  = new Scene(pane);
            
            URL url = SonarApplication.class.getResource("/style.css");
            scene.getStylesheets().add(url.toString());     
            
            stage.setScene(scene);
            stage.show();
            
        } catch (Exception e) {
        	logger.error("Exception launching the application: {}", e.getMessage());
        }	
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void stop(){
		controller.close();
	}
	

}
