package com.sonar.trading.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sonar.trading.model.DiffOrder;
import com.sonar.trading.model.PositionType;
import com.sonar.trading.model.Trade;
import com.sonar.trading.service.SonarService;
import com.sonar.trading.util.PropertiesUtil;
import com.sun.javafx.charts.Legend;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

/**
 * Controller class responsible for handling all data coming from the service layer to be shown
 * by the JavaFX application. This controller implements Observer interface what means it will be
 * notified by its observables
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class SonarController implements Observer {
	
	private static final Logger logger = LoggerFactory.getLogger(SonarController.class);
    	
	private final int seriesStartPoint = 1;
	private int xSeriesBuyData = seriesStartPoint;
	private int xSeriesSellData = seriesStartPoint;
	private int xSeriesTradeData = seriesStartPoint;
	
	// Setting this upper limit because is very difficult to view them in the graph
	private final int SHOWN_UPPER_LIMIT_RECORDS = Integer.parseInt(PropertiesUtil.get("default.upper.limit"));		
	
	private DiffOrder selectedOrder;
	private Trade selectedTrade;
	private Map<String, XYChart.Data<Number, Number>> orderNodesMap = new HashMap<>();
	private Map<Long, XYChart.Data<Number, Number>> tradeNodesMap = new HashMap<>();
		
	private final String buySelectedNodeStyle 	= "-fx-background-color: green; -fx-background-radius: 3px; -fx-padding: 3px;";
	private final String buyUnselectedNodeStyle = "-fx-background-color: green, white; -fx-stroke-width: 2px; -fx-effect: null;";
	private final String buySelectedLabelNodeStyle   = "-fx-background-color: green, white; fx-padding: 20px; -fx-stroke: blue;";
	
	private final String sellSelectedNodeStyle 	 = "-fx-background-color: red; -fx-background-radius: 3px; -fx-padding: 3px;";
	private final String sellUnselectedNodeStyle = "-fx-background-color: red, white; -fx-stroke-width: 2px; -fx-effect: null;";
	private final String sellSelectedLabelNodeStyle   = "-fx-background-color: red, white; fx-padding: 20px; -fx-stroke: blue;";
	
	private final String tradeSelectedNodeStyle   = "-fx-background-color: blue; -fx-background-radius: 3px; -fx-padding: 3px;";
	private final String tradeUnselectedNodeStyle = "-fx-background-color: blue, white; -fx-stroke-width: 2px; -fx-effect: null;";
	private final String tradeSelectedLabelNodeStyle   = "-fx-background-color: blue, white; fx-padding: 20px; -fx-stroke: blue;";
	
	private final String tradeBuyNodeStyle   = "-fx-background-color: green; -fx-background-radius: 3px; -fx-padding: 3px;";
	private final String tradeSellNodeStyle  = "-fx-background-color: red; -fx-background-radius: 3px; -fx-padding: 3px;";
	
	private final String unSelectedLabelNodeStyle   = "-fx-background-color: grey;";
	
	
	
	private Map<String, Integer> idPositionOrderMap = new HashMap<>();
	private Map<Long, Integer> idPositionTradeMap = new HashMap<>();

	private List<XYChart.Data<Number, Number>> buyNodes = new ArrayList<>();
	private List<XYChart.Data<Number, Number>> sellNodes = new ArrayList<>();
	private List<XYChart.Data<Number, Number>> tradeNodes = new ArrayList<>();

	private XYChart.Series<Number, Number> buySeries = new XYChart.Series<>();
	private XYChart.Series<Number, Number> sellSeries = new XYChart.Series<>();
	private XYChart.Series<Number, Number> tradeSeries = new XYChart.Series<>();
	
	private boolean resetBuyGraph = false;
	private boolean resetSellGraph = false;
	private Map<String, Boolean> seriesVisibility = new HashMap<>();
	
	private final String buyLabelName = "Buys/Bids";
	private final String sellLabelName = "Sells/Asks";
	private final String tradeLabelName = "Trades";
	
	private List<Trade> trades = new ArrayList<>();
	
    @FXML
    private NumberAxis xAxis;

    @FXML
    private NumberAxis yAxis;

    @FXML
	private TextField maxOrderField;
    
    @FXML
	private TextField upSticksCounterField;
    
    @FXML
	private TextField downSticksCounterField;
    
    // Order details labels
    @FXML
	private Label timestamp;
	
	@FXML
	private Label rate;
	
	@FXML
	private Label type;
	
	@FXML
	private Label status;
	
	@FXML
	private Label amount;
	
	@FXML
	private Label value;
	
	@FXML
	private Label id;
	
	@FXML
	private Label currentOrderPosition;
    
	// Trade details labels
	@FXML
	private Label createdAt;
	
	@FXML
	private Label tradeAmount;
	
	@FXML
	private Label makerSide;
	
	@FXML
	private Label price;
	
	@FXML
	private Label tid;
	
	@FXML
	private Label currentTradePosition;
	
	@FXML
	private Label strategyPosition;
	
	
    @FXML
    public LineChart<Number, Number> lineChart;

    private SonarService service = SonarService.getInstance();
    
    
    public void initialize(){  
    	Integer maxOrders = service.getMaxOrders();
				
		// Line chart configuration
		lineChart.setAnimated(false);
		lineChart.setTitle("Sonar Trading Real-Time Monitor");
		lineChart.setHorizontalGridLinesVisible(true);	
		
		// Setting xAxis limits
		xAxis.setAutoRanging(false);
		xAxis.setLowerBound(seriesStartPoint);
		xAxis.setUpperBound(maxOrders);
		xAxis.setTickUnit(0);
		xAxis.setMinorTickVisible(false);
		
		
		// Set Name for Series
		buySeries.setName(buyLabelName);		
		sellSeries.setName(sellLabelName);
		tradeSeries.setName(tradeLabelName);
		
		// Filling input field with default maxOrders
		maxOrderField.setText(maxOrders.toString());
		
		upSticksCounterField.setText(""+service.getUpTicksCount());
		downSticksCounterField.setText(""+service.getDownTicksCount());
		
		
		// Add Chart Series
		lineChart.getData().addAll(buySeries, sellSeries, tradeSeries);
		
		initLegend();
		
		service.addObserver(this);
		service.init();			
    }
    
        
    private void changeLabelStyle(String labelName, Node node, boolean visible) {
    	if( visible ) {
    		node.setStyle(unSelectedLabelNodeStyle);  
    		
    		switch (labelName) {
				case tradeLabelName:
					resetTradeLabels();					
					break;
				default:
					resetOrderLabels();
					break;
			}
    	}else {
    		switch (labelName) {
				case buyLabelName:
					node.setStyle(buySelectedLabelNodeStyle);
					break;
				case sellLabelName:
					node.setStyle(sellSelectedLabelNodeStyle);
					break;
				default:
					node.setStyle(tradeSelectedLabelNodeStyle);
					break;
			}
    	}
    	seriesVisibility.put(labelName, visible);
    }
        
    
    private void initLegend() {   	
    	lineChart.getChildrenUnmodifiable().stream().forEach(node->{
    		if (node instanceof Legend) {
		        Legend legend = (Legend) node;
		        
		        legend.getItems().stream().forEach(l ->{
		        	lineChart.getData().stream().forEach(s->{
		        		if (s.getName().equals(l.getText())) {
		        			seriesVisibility.put(s.getName(), Boolean.TRUE);
			        		
		                    l.getSymbol().setCursor(Cursor.HAND); // Hint user that legend symbol is clickable
		                    l.getSymbol().setOnMouseClicked(me -> {
		                        if (me.getButton() == MouseButton.PRIMARY) {
		                        	boolean visible = s.getNode().isVisible();		                            
		                        	s.getNode().setVisible(!visible); // Toggle visibility of line
		                            
		                        	changeLabelStyle(s.getName(), l.getSymbol(), visible);
		                        	
		                            s.getData().stream().forEach(d->{
		                            	if (d.getNode() != null) {
		                                    d.getNode().setVisible(s.getNode().isVisible()); // Toggle visibility of every node in the series
		                                }
		                            });
		                            
		                            seriesVisibility.put(s.getName(), s.getNode().isVisible());
		                        }
		                    });
		                }
		        	});			        	
		        });				        
		    }
		});   	
    	
    }
    
    /**
     * This method refreshes all data related to asks/bids/trades when button "Refresh" is triggered, making a rest api call
     * to get the diff-orders data and the trades data
     */
    @FXML
	public void refreshData() {  
    	String input = null;
    	try {
    		input = maxOrderField.getText();
    		int numOfOrders = Integer.parseInt(input);
    		
    		if( numOfOrders > 0 && numOfOrders <= SHOWN_UPPER_LIMIT_RECORDS) {
    			
    			// Reseting the graph just when the input is different that the existing 
    			// numOfOrders to avoid unnecessary process
	    		if( service.getMaxOrders() != numOfOrders ) {
	    			service.setMaxOrders(numOfOrders);
	    			service.setProcessRealTimeMessages(false);
    	    		
    	    		xAxis.setUpperBound(service.getMaxOrders());
    	    		
    	    		resetOrderLabels();
    	    		resetTradeLabels();
    	    		
    	    		service.init();	
	    		}     			
    		}else {
    			logger.warn("Bids/Asks value {} must be > 0 and <= {}", input, SHOWN_UPPER_LIMIT_RECORDS);
    		}
    		
    	}catch(NumberFormatException e) {
    		logger.error("Invalid Bids/Asks input value '{}'", input);
    	}		
	}
        
    /**
     * This method refreshes only the data related to trades when button "Update" is triggered, making a rest api call
     * to get only the trades data
     */
    @FXML
   	public void refreshSticks() {    	
       	try {
       		int upSticksCounter = Integer.parseInt(upSticksCounterField.getText());
       		int downSticksCounter = Integer.parseInt(downSticksCounterField.getText());
       		
       		if( upSticksCounter > 0 && downSticksCounter > 0 && 
       				upSticksCounter <= SHOWN_UPPER_LIMIT_RECORDS && 
       						downSticksCounter <= SHOWN_UPPER_LIMIT_RECORDS) {
       			logger.info("Setting upSticksCounter(M)={}, downSticksCounter(N)={}", upSticksCounter, downSticksCounter);
       			       			
       			service.setUpTicksCount(upSticksCounter);
       			service.setDownTicksCount(downSticksCounter);
   	    		
       			service.processTradesData();       			
       		}else {
       			logger.warn("upSticksCounter/downSticksCounter value must be > 0 and <= {}", SHOWN_UPPER_LIMIT_RECORDS);
       		}
       		
       	}catch(NumberFormatException e) {
       		logger.error("Impossible to parse upSticksCounter/downSticksCounter values");
       	}		
   	}
   
    /**
     * Receives notifications once the diff-orders/trades are collected from the rest api.
     */
    @Override
	public void update(Observable o, Object arg) {
    	
    	if( arg instanceof DiffOrder[] ) {
    		List<DiffOrder> orders = Arrays.asList(((DiffOrder[]) arg));
    		Platform.runLater(() -> {			
    			// Update buys/sells graphs.
    			addOrdersDataToSeries(orders);
    		});
    	}else {
    		List<Trade> trades = Arrays.asList(((Trade[]) arg));
    		Platform.runLater(() -> {			
    			// Update trades graph.
    			addTradesDataToSeries(trades);   			
    		});
    	}
	}
	
    private  void addTradesDataToSeries(List<Trade> trades) {
    	if( !trades.isEmpty() ) {
    		idPositionTradeMap.clear();
			tradeNodes.clear();
			xSeriesTradeData = seriesStartPoint;
		}
		
    	trades.stream().forEach(t -> {
			int position;
			XYChart.Data<Number, Number> node = null;
						
			position = xSeriesTradeData;
			node = new XYChart.Data<>(position, t.getPrice());
			idPositionTradeMap.put(t.getTid(), position);
			tradeNodes.add(new SelectedTradeNode(t, node).tradeNode);
			
			tradeNodesMap.put(t.getTid(), node);
			
			node.setExtraValue(t.getStrategyPosition());
			
			xSeriesTradeData++;			
		});    	
		
    	// Repaint trade graph only if it is visible
		if( seriesVisibility.get(tradeLabelName) ) {
    		tradeSeries.getData().clear();
    		tradeNodes.stream().forEach(point -> {
    			
    			// Setting style for trades tagged by strategy as BUY or SELL
    			PositionType type = (PositionType) point.getExtraValue();
    			if( PositionType.BUY.equals(type) ) {    				
    				point.getNode().setStyle(tradeBuyNodeStyle);    				
    			}else if( PositionType.SELL.equals(type) ) {
    				point.getNode().setStyle(tradeSellNodeStyle);  
    			}
    			
    			tradeSeries.getData().add(point);
    		});	
    		
    		refreshTradeLabels();
    		
    		// Set style for current selected node			
    		setTradeNodeStyle(true);
    	}
		
	}
    
    private  void addOrdersDataToSeries(List<DiffOrder> orders) {
		if( !orders.isEmpty() ) {
			idPositionOrderMap.clear();
			orderNodesMap.clear();
		}
		
		orders.stream().forEach(o -> {
			int position;
			XYChart.Data<Number, Number> node = null;
			
			if (PositionType.BUY.equals(o.getType())) {
				if( !resetBuyGraph ) {
					buyNodes.clear();
					xSeriesBuyData = seriesStartPoint;
					resetBuyGraph = true;
				}
				
				position = xSeriesBuyData;
				node = new XYChart.Data<>(position, o.getRate());
				idPositionOrderMap.put(o.getId(), position);
				buyNodes.add(new SelectedOrderNode(o, node).orderNode);
				
				xSeriesBuyData++;
				
			}else {
				if( !resetSellGraph ) {
					sellNodes.clear();
					xSeriesSellData = seriesStartPoint;
					resetSellGraph = true;
				}	
				
				position = xSeriesSellData;
				node = new XYChart.Data<>(position, o.getRate());
				idPositionOrderMap.put(o.getId(), position);
				sellNodes.add(new SelectedOrderNode(o, node).orderNode);
				
				xSeriesSellData++;		
			}			
			orderNodesMap.put(o.getId(), node);
		});
		
		// Repaint buy graph only if it is visible
		if( resetBuyGraph && seriesVisibility.get(buyLabelName) ) {
			buySeries.getData().clear();
			buyNodes.stream().forEach(point -> buySeries.getData().add(point));	
		}		
		
		// Repaint sell graph only if it is visible
		if( resetSellGraph && seriesVisibility.get(sellLabelName) ) {
			sellSeries.getData().clear();
			sellNodes.stream().forEach(point -> sellSeries.getData().add(point));
		}
		
		resetBuyGraph = false;
		resetSellGraph = false;
		
		// Only if buy or sell graph is visible
		if( seriesVisibility.get(buyLabelName) || seriesVisibility.get(sellLabelName) ) {
			refreshOrderLabels();
			
			// Set style for current selected node			
			setOrderNodeStyle(true);
		}
		
	}
	
	
	private XYChart.Data<Number, Number> findSelectedOrderNode() {
		if( selectedOrder != null ) {
			return orderNodesMap.get(selectedOrder.getId());
		}
		return null;
	}
	
	private XYChart.Data<Number, Number> findSelectedTradeNode() {
		if( selectedTrade != null ) {
			return tradeNodesMap.get(selectedTrade.getTid());
		}
		return null;
	}
	
	public void close() {
		service.close();
	}
		
	private void refreshOrderLabels() {
		if( selectedOrder != null ) {
			String selectedOrderId = selectedOrder.getId();
			if( !idPositionOrderMap.containsKey(selectedOrderId) ) {
				Platform.runLater(() -> {			
					timestamp.setText("");
					rate.setText("");
					type.setText("");
					status.setText("");
					amount.setText("");
					value.setText("");
					id.setText("");
					currentOrderPosition.setText("");
				});	
				
				selectedOrder = null;
				
			}else {
				Integer selectedPosition = getSelectedOrderPosition();
				if( currentOrderPosition != null && selectedPosition != null ) {
					Platform.runLater(() -> {
						currentOrderPosition.setText(selectedPosition.toString());
					});
				}				
			}			
		}
	}
	
	private void resetOrderLabels() {
		if( selectedOrder != null ) {
			String selectedOrderId = selectedOrder.getId();
			if( idPositionOrderMap.containsKey(selectedOrderId) ) {
				Platform.runLater(() -> {			
					timestamp.setText("");
					rate.setText("");
					type.setText("");
					status.setText("");
					amount.setText("");
					value.setText("");
					id.setText("");
					currentOrderPosition.setText("");
				});	
				
				setOrderNodeStyle(false);
				
				selectedOrder = null;
				idPositionOrderMap.remove(selectedOrderId);
				
			}	
		}
	}
		
	private void refreshTradeLabels() {
		if( selectedTrade != null ) {
			Long selectedTradeId = selectedTrade.getTid();
			if( !idPositionTradeMap.containsKey(selectedTradeId) ) {
				Platform.runLater(() -> {			
					createdAt.setText("");
					tradeAmount.setText("");
					makerSide.setText("");
					price.setText("");
					tid.setText("");
					currentTradePosition.setText("");
					strategyPosition.setText("");
				});	
				
				selectedTrade = null;
				
			}else {
				Integer selectedPosition = getSelectedTradePosition();
				if( currentTradePosition != null && selectedPosition != null ) {
					Platform.runLater(() -> {
						currentTradePosition.setText(selectedPosition.toString());
					});
				}	
			}			
		}
	}
	
	private void resetTradeLabels() {
		if( selectedTrade != null ) {
			Long selectedTradeId = selectedTrade.getTid();
			if( idPositionTradeMap.containsKey(selectedTradeId) ) {
				Platform.runLater(() -> {			
					createdAt.setText("");
					tradeAmount.setText("");
					makerSide.setText("");
					price.setText("");
					tid.setText("");
					currentTradePosition.setText("");
					strategyPosition.setText("");
				});	
				
				setTradeNodeStyle(false);
				
				selectedTrade = null;
				idPositionTradeMap.remove(selectedTradeId);
			}	
		}
	}
	
	private Integer getSelectedOrderPosition() {
		if( selectedOrder != null ) {
			return idPositionOrderMap.get(selectedOrder.getId());
		}
		return null;
	}
	
	private Integer getSelectedTradePosition() {
		if( selectedTrade != null ) {
			return idPositionTradeMap.get(selectedTrade.getTid());
		}
		return null;
	}
	
	private void setOrderNodeStyle(boolean selected) {
		if( selectedOrder != null ) {
			String style = "";
			if( PositionType.BUY.equals(selectedOrder.getType()) ) {				
				if(selected) {
					style = buySelectedNodeStyle;
				}else {
					style = buyUnselectedNodeStyle;
				}				
			}else {
				if(selected) {
					style = sellSelectedNodeStyle;
				}else {
					style = sellUnselectedNodeStyle;
				}				
			}
			
			XYChart.Data<Number, Number> selectedNode = findSelectedOrderNode();
			if( selectedNode != null ) {
				Node found = selectedNode.getNode().lookup(".chart-line-symbol");
				if( found != null ) {
					found.setStyle(style);
				}
			}else {
				logger.warn("Selected order {} not found", selectedOrder.getId());
			}
		}			
	}
	
	private void setTradeNodeStyle(boolean selected) {
		if( selectedTrade != null ) {
			String style = "";
			if(selected) {
				style = tradeSelectedNodeStyle;
			}else {
				style = tradeUnselectedNodeStyle;
			}	
			
			XYChart.Data<Number, Number> selectedNode = findSelectedTradeNode();
			if( selectedNode != null ) {
				Node found = selectedNode.getNode().lookup(".chart-line-symbol");
				if( found != null ) {
					// Only not tagged trades by strategy will update the style
					if( selectedNode.getExtraValue() == null) {
						found.setStyle(style);
					}
				}
			}else {
				logger.warn("Selected trade {} not found", selectedTrade.getTid());
			}
		}			
	}


	/**
	 * Class to implement mouse actions such as onClick to show orders details on the pane
	 * @author Jorge Marin Guerrero
	 *
	 */
	private class SelectedOrderNode extends StackPane {
		private XYChart.Data<Number, Number> orderNode;

		private SelectedOrderNode(DiffOrder order, XYChart.Data<Number, Number> currentNode) {
			orderNode = currentNode;
			orderNode.setNode(this);
						
			setPrefSize(10, 10);

			setOnMouseEntered(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent mouseEvent) {
					setCursor(Cursor.HAND);
					toFront();
				}
			});
						
			setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent mouseEvent) {
					getChildren().clear();
					
					// Reset style for previous selected node					
					setOrderNodeStyle(false);
					
					// Update selected one
					selectedOrder = order;	
					
					// Set style for current selected node			
					setOrderNodeStyle(true);
					
					printSelectedOrder();	
				}
			});
		}
		
		private void printSelectedOrder() {
			Platform.runLater(() -> {			
				timestamp.setText(selectedOrder.getTimestamp().toString().split("\\.")[0]);
				rate.setText(selectedOrder.getRate().toPlainString());
				type.setText(selectedOrder.getType().name());
				status.setText(selectedOrder.getStatus().name());
				amount.setText(selectedOrder.getAmount().toPlainString());
				value.setText(selectedOrder.getValue().toPlainString());
				id.setText(selectedOrder.getId());	
				
				Integer selectedOrderPosition = getSelectedOrderPosition();
				if( selectedOrderPosition != null ) {
					currentOrderPosition.setText(selectedOrderPosition.toString());
				}
			});		
		}

	}
	
	/**
	 * Class to implement mouse actions such as onClick to show trades details on the pane
	 * @author Jorge Marin Guerrero
	 *
	 */
	private class SelectedTradeNode extends StackPane {
		private XYChart.Data<Number, Number> tradeNode;

		private SelectedTradeNode(Trade trade, XYChart.Data<Number, Number> currentNode) {
			tradeNode = currentNode;
			tradeNode.setNode(this);
						
			setPrefSize(10, 10);

			setOnMouseEntered(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent mouseEvent) {
					setCursor(Cursor.HAND);
					toFront();
				}
			});
						
			setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent mouseEvent) {
					getChildren().clear();
					
					// Reset style for previous selected node					
					setTradeNodeStyle(false);
						
					// Update selected one
					selectedTrade = trade;	
					
					// Set style for current selected node			
					setTradeNodeStyle(true);
					
					printSelectedTrade();	
				}
			});
		}
		
		private void printSelectedTrade() {
			Platform.runLater(() -> {	
				createdAt.setText(selectedTrade.getCreatedAt().toLocalDateTime().toString());//.toString().split("\\.")[0]);
				tradeAmount.setText(selectedTrade.getAmount().toPlainString());
				makerSide.setText(selectedTrade.getMakerSide().name());
				price.setText(selectedTrade.getPrice().toPlainString());
				tid.setText(selectedTrade.getTid().toString());
				
				if( selectedTrade.getStrategyPosition() != null ) {
					strategyPosition.setText(selectedTrade.getStrategyPosition().toString());
				}else {
					strategyPosition.setText("-");
				}
				
				
				Integer selectedTradePosition = getSelectedTradePosition();
				if( selectedTradePosition != null ) {
					currentTradePosition.setText(selectedTradePosition.toString());
				}
			});		
		}

	}
}
