package com.sonar.trading.model;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


public class DiffOrder {

	@JsonProperty("d")
	@JsonDeserialize(using = UnixTimestampJsonDeserializer.class)
	private ZonedDateTime timestamp;

	@JsonProperty("r")
	private BigDecimal rate;

	@JsonProperty("t")
	@JsonDeserialize(using = PositionTypeJsonDeserializer.class)
	private PositionType type;
	
	@JsonProperty("s")
	@JsonDeserialize(using = StatusTypeJsonDeserializer.class)
	private StatusType status;

	@JsonProperty("a")
	private BigDecimal amount;

	@JsonProperty("v")
	private BigDecimal value;

	@JsonProperty("o")
	private String id;
	
	// To avoid any instantiation
	private DiffOrder() {}

	private static class PositionTypeJsonDeserializer extends JsonDeserializer<PositionType> {
		private final static String ONE = "1";

		@Override
		public PositionType deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			return p.getText().equals(ONE) ? PositionType.SELL : PositionType.BUY;
		}
	}
	
	private static class StatusTypeJsonDeserializer extends JsonDeserializer<StatusType> {
		@Override
		public StatusType deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			try {
				return StatusType.valueOf(p.getText().toUpperCase());
			} catch (IllegalArgumentException e) {
				return StatusType.UNKNWOWN;
			}
		}
	}
	
	private static class UnixTimestampJsonDeserializer extends JsonDeserializer<ZonedDateTime> {
		private final static String UTC = "UTC";

		@Override
		public ZonedDateTime deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			return ZonedDateTime.ofInstant(Instant.ofEpochMilli(Long.valueOf(p.getText())), ZoneId.of(UTC));
		}
	}

	// Getters
	public ZonedDateTime getTimestamp() {
		return timestamp;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public PositionType getType() {
		return type;
	}
	
	public StatusType getStatus() {
		return status;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public BigDecimal getValue() {
		return value;
	}

	public String getId() {
		return id;
	}


	@Override
	public String toString() {
		return "DiffOrder [timestamp=" + timestamp + ", rate=" + rate + ", type=" + type + ", status=" + status
				+ ", amount=" + amount + ", value=" + value + ", id=" + id + "]";
	}

	public static class DiffOrderWrapper {

		@JsonProperty
		private String type;

		@JsonProperty
		private String book;

		@JsonProperty
		private BigInteger sequence;

		@JsonProperty("payload")
		private List<DiffOrder> diffOrders;

		// To avoid any instantiation
		private DiffOrderWrapper() {}
		
		// Getters
		public String getType() {
			return type;
		}

		public String getBook() {
			return book;
		}

		public BigInteger getSequence() {
			return sequence;
		}

		public List<DiffOrder> getDiffOrders() {
			return diffOrders;
		}

	}
}
