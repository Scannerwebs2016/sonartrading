package com.sonar.trading.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Order {
	
	@JsonProperty
	private String book;
	
	@JsonProperty
	private BigDecimal price;
	
	@JsonProperty
	private BigDecimal amount;
	
	@JsonProperty
	private String oid;
	
	// To avoid any instantiation
	private Order() {}

	// Getters
	public String getBook() {
		return book;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public String getOid() {
		return oid;
	}
}
