package com.sonar.trading.model;

import java.io.IOException;
import java.math.BigInteger;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


public class OrderBook {
	
	@JsonProperty("updated_at")
	@JsonDeserialize(using = ISOOffsetDateTimeJsonDeserializer.class)
	private ZonedDateTime updatedAt;
	
	@JsonProperty
	private List<Order> bids;
	
	@JsonProperty
	private List<Order> asks;
			
	@JsonProperty
	private BigInteger sequence;
	
	// To avoid any instantiation
	private OrderBook() {}

	
	// Getters
	public ZonedDateTime getUpdatedAt() {
		return updatedAt;
	}

	public List<Order> getBids() {
		return bids;
	}

	public List<Order> getAsks() {
		return asks;
	}

	public BigInteger getSequence() {
		return sequence;
	}


	private static class ISOOffsetDateTimeJsonDeserializer extends JsonDeserializer<ZonedDateTime> {
		@Override
		public ZonedDateTime deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			return ZonedDateTime.parse(p.getText(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		}
	}
	

	public static class OrderBookWrapper {
		
		@JsonProperty
		private Boolean success;
		
		@JsonProperty("payload")
		private OrderBook orderBook;

		public Boolean getSuccess() {
			return success;
		}

		public OrderBook getOrderBook() {
			return orderBook;
		}
		
		// To avoid any instantiation
		private OrderBookWrapper() {
			
		}
	}
}
