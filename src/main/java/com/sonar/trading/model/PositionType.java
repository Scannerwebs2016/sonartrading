package com.sonar.trading.model;

public enum PositionType {
	SELL,
	BUY;
}
