package com.sonar.trading.model;

public enum StatusType {
	OPEN, 
    PARTIALLY_FILLED, 
    COMPLETED, 
    CANCELLED,
    UNKNWOWN;
}
