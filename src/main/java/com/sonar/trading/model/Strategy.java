/**
 * 
 */
package com.sonar.trading.model;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to implement the buy/sell strategy logic after counting a number of consecutive downTicks/upTicks
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class Strategy {	
	private static final Logger logger = LoggerFactory.getLogger(Strategy.class);
	
	private int upTicksCount;
	private int downTicksCount;	
	
	private int ticks = 1;
	private int lastTick = 0;
	private Trade lastTrade = null;
	
	public Strategy(int upTicksCount, int downTicksCount) {
		this.upTicksCount = upTicksCount;
		this.downTicksCount = downTicksCount;
	}
	
		
	public int getUpTicksCount() {
		return upTicksCount;
	}

	public int getDownTicksCount() {
		return downTicksCount;
	}

	public void setUpTicksCount(int upTicksCount) {
		this.upTicksCount = upTicksCount;
	}

	public void setDownTicksCount(int downTicksCount) {
		this.downTicksCount = downTicksCount;
	}


	public void evaluate(List<Trade> trades) {	
		logger.info("upTicksCount: {} downTicksCount: {}", upTicksCount, downTicksCount);
		
		trades.forEach(t->{
			int tick = 0;
			if ( lastTrade != null ) {
				
				tick = t.getPrice().compareTo(lastTrade.getPrice());
				
				if (0 == tick) {
					// ZERO
					// nothing to do
				} else if (tick == lastTick) {  
					// UP/DOWN
					ticks += 1;
				} else {
					// Reset ticks
					ticks = 1;
				}
								
				switch(tick) {
					case -1:
						// DOWN
						if( ticks == downTicksCount) {
							ticks = 0;
							t.setTtrategyPosition(PositionType.BUY);
							
							logger.info("Trade {} tagged by strategy as {}", t.getTid(), t.getStrategyPosition().name());
						}
						break;
					case 1:
						//UP
						if( ticks == upTicksCount) {
							ticks = 0;
							t.setTtrategyPosition(PositionType.SELL);
							
							logger.info("Trade {} tagged by strategy as {}", t.getTid(), t.getStrategyPosition().name());
						}
						break;
				}				
			}
			lastTick = tick;
			lastTrade = t;			
		});		
	}
	
}
