package com.sonar.trading.model;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


public class Trade {
	
	@JsonProperty
	private String book;
	    
	@JsonProperty("created_at")
	@JsonDeserialize(using = PatternedDateTimeJsonDeserializers.class)
	private ZonedDateTime createdAt;
	
	@JsonProperty
	private BigDecimal amount;
	
	@JsonProperty("maker_side")
	@JsonDeserialize(using =PositionTypeJsonDeserializer.class)
	private PositionType makerSide;
	
	@JsonProperty
	private BigDecimal price;
	
	@JsonProperty
	private Long tid;
	
	@JsonIgnore
	private PositionType strategyPosition;
	
	public void setTtrategyPosition(PositionType strategyPosition) {
		this.strategyPosition = strategyPosition;
	}
	
	public PositionType getStrategyPosition() {
		return strategyPosition;
	}



	public void setStrategyPosition(PositionType strategyPosition) {
		this.strategyPosition = strategyPosition;
	}



	// To avoid any instantiation
	@SuppressWarnings("unused")
	private Trade() {}
	
	// Used just for testing strategy
	Trade(BigDecimal price) {
		this.price = price;
	}

	// Getters	
	public String getBook() {
		return book;
	}

	public ZonedDateTime getCreatedAt() {
		return createdAt;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public PositionType getMakerSide() {
		return makerSide;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public Long getTid() {
		return tid;
	}




	private static class PositionTypeJsonDeserializer extends JsonDeserializer<PositionType> {
		private static final String SELL = "sell";
		
		@Override
		public PositionType deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			return p.getText().equals(SELL) ? PositionType.SELL : PositionType.BUY;
		}
	}	
	
	private static class PatternedDateTimeJsonDeserializers extends JsonDeserializer<ZonedDateTime> {
		private static final String yyyy_MM_dd_HH_mm_ssZ = "yyyy-MM-dd'T'HH:mm:ssZ";
		
		@Override
		public ZonedDateTime deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			return ZonedDateTime.parse(p.getText(), DateTimeFormatter.ofPattern(yyyy_MM_dd_HH_mm_ssZ));
		}
		
	}
	
	
	
	public static class TradeWrapper {
		
		private Boolean success;
		
		@JsonProperty("payload")
		private List<Trade> trades;

		// To avoid any instantiation
		private TradeWrapper() {}
		
		public boolean getSuccess() {
			return success;
		}

		public List<Trade> getTrades() {
			return trades;
		}
		
		
	}
	

}
