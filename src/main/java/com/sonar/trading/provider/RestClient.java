package com.sonar.trading.provider;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sonar.trading.model.OrderBook;
import com.sonar.trading.model.OrderBook.OrderBookWrapper;
import com.sonar.trading.model.Trade;
import com.sonar.trading.model.Trade.TradeWrapper;
import com.sonar.trading.util.PropertiesUtil;


/**
 * Singleton Rest client for connecting to the bitso REST API to get order books and trades
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class RestClient {
	
	private static final Logger logger = LoggerFactory.getLogger(RestClient.class);
	
	// These are protected to make them visible/mockable in the test cases
	protected static final String BASE_URL = PropertiesUtil.get("api.base.url");
	protected static final String ENDPOINT_ORDERBOOK = PropertiesUtil.get("api.endpoint.orderbook");
	protected static final String ENDPOINT_TRADES = PropertiesUtil.get("api.endpoint.trades");
	
	protected static ResteasyClient httpClient;
	private static RestClient instance;
	
	
	private RestClient() {
		httpClient = new ResteasyClientBuilder().build();
		logger.info("REST api client initialized");		
	}
	
	public static RestClient getInstance() {
		if ( instance == null ) {
			instance = new RestClient();
		}
		return instance;
	}	

	public void close() {			
		httpClient.close();
		instance = null;
		logger.info("REST api client closed");	
	}
	
	/**
	 * This method returns a list of all open orders in the specified book. 
	 * If the aggregate parameter is set to true, orders will be aggregated by price, 
	 * and the response will only include the top 50 orders for each side of the book. 
	 * If the aggregate parameter is set to false, the response will include the full order book.
	 * 
	 * @param book Specifies which book to use
	 * @param aggregate	(optional, default true) Specifies if orders should be aggregated by price.
	 * 
	 * @return
	 */
	public OrderBook getOrderBook(String book, Optional<Boolean> aggregate) {
		Response response = null;		
		ResteasyWebTarget target = httpClient.target(BASE_URL).path(ENDPOINT_ORDERBOOK)
				.queryParam("book", book);
		
		if( aggregate.isPresent() ) {
			target = target.queryParam("aggregate", aggregate.get());
		}else {
			// Default value is true
			target = target.queryParam("aggregate", true);
		}		
		
		logger.info("REST api call to {} - started", target.getUri().toString());
				
		try {
			response = target.request().get();
			logger.info("REST api call to {} - ended", target.getUri().toString());
			
			OrderBookWrapper wrapper = new ObjectMapper().readerFor(OrderBookWrapper.class).readValue(response.readEntity(String.class));
			if( wrapper.getSuccess() ) {
				return wrapper.getOrderBook();
			}else {
				throw new Exception("Response was not success");
			}			
		} catch (Exception e) {
			String errorMsg = "Exception getting order books for "+book+": "+e.getMessage();
			logger.error(errorMsg);
			throw new RuntimeException(errorMsg);
		} finally {
			if( response != null ) {
				response.close();
			}			 
		}
	}
	
		
	/**
	 * This method returns a list of recent trades from the specified book.
	 * 
	 * @param book Specifies which book to use
	 * @param marker (optional) Returns objects that are older or newer (depending on 'sort’) than the object with this ID
	 * @param sort (optional, default desc) Specifies ordering direction of returned objects ('asc’, 'desc’)
	 * @param limit (optional, default 25) Specifies number of objects to return. (Max is 100)
	 * @return
	 */
	public List<Trade> getTrades(String book, Optional<Long> marker, Optional<String> sort, Optional<Integer> limit) {
		Response response = null;		
		ResteasyWebTarget target = httpClient.target(BASE_URL).path(ENDPOINT_TRADES)
				.queryParam("book", book);
		
		if( marker.isPresent() ) 
			target = target.queryParam("marker", marker);
		if( sort.isPresent() ) 
			target = target.queryParam("sort", sort);
		if( limit.isPresent() )	
			target = target.queryParam("limit", limit);		
		
		logger.info("REST api call to {} - started", target.getUri().toString());
				
		try {
			response = target.request().get();
			logger.info("REST api call to {} - ended", target.getUri().toString());
			
			TradeWrapper wrapper = new ObjectMapper().readerFor(TradeWrapper.class).readValue(response.readEntity(String.class));
			if( wrapper.getSuccess() ) {
				return wrapper.getTrades();
			}else {
				throw new Exception("Response was not success");
			}	
		} catch (Exception e) {
			String errorMsg = "Exception getting trades for "+book+": "+e.getMessage();
			logger.error(errorMsg);
			throw new RuntimeException(errorMsg);
		} finally {
			if( response != null ) {
				response.close();
			}			 
		}
	}	

}
