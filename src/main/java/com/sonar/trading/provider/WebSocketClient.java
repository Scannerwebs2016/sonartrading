/**
 * 
 */
package com.sonar.trading.provider;

import java.net.URI;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;

import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sonar.trading.model.DiffOrder.DiffOrderWrapper;
import com.sonar.trading.service.SonarService;
import com.sonar.trading.util.PropertiesUtil;


/**
 * Singleton class to receive messages from diff-orders channel via web socket
 * 
 * @author Jorge Marin Guerrero
 *
 */
@WebSocket
public class WebSocketClient {
	
	private static final Logger logger = LoggerFactory.getLogger(WebSocketClient.class);	
	
	private final static String WEBSOCKET_URL = PropertiesUtil.get("websocket.url");
	private final static String SUBSCRIPTION_REQUEST = "{ \"action\": \"subscribe\", \"book\": \"btc_mxn\", \"type\": \"diff-orders\" }";
	private final static String SUBSCRIPTION_TYPE_MESSAGE = "\"action\":\"subscribe\"";
	private final static String DIFF_ORDER_TYPE_MESSAGE = "\"type\":\"diff-orders\"";
	
	private ConcurrentLinkedQueue<DiffOrderWrapper> queue = new ConcurrentLinkedQueue<DiffOrderWrapper>();
	
	private org.eclipse.jetty.websocket.client.WebSocketClient webSocketClient = null;
	private static WebSocketClient instance;
	
	private Session session = null;
	
	
	public static WebSocketClient getInstance() {
		if(null == instance) {
			instance = new WebSocketClient();
		}
		return instance;
	}
	
	private WebSocketClient() {
		webSocketClient = new org.eclipse.jetty.websocket.client.WebSocketClient();
		
		connet();
	}	
	
	
	private void connet() {	
		try {
			logger.info("WebSocket client connecting to {}", WEBSOCKET_URL);
			
			webSocketClient.start();
			
			// 1 - Subscribe to diff-orders channe
			Future<Session> futureSession = webSocketClient.connect(this, URI.create(WEBSOCKET_URL));
			session = futureSession.get();			
			
			RemoteEndpoint endpoint = session.getRemote();
			endpoint.sendString(SUBSCRIPTION_REQUEST);
			endpoint.flush();
			logger.info("WebSocket client listening ...");
			
		} catch (Exception e) {
			logger.error("Error initializing Bitso WebSocket client", e);
		}
	}
	
	
	
	public void close() {			
		try {
			webSocketClient.stop();
			session.close();
			instance = null;
			
			logger.info("WebSocket client closed");	
		} catch (Exception e) {
			logger.error("Exception closing Websocket client: {}", e.getMessage());
		}
	}
		
	
	@OnWebSocketMessage
	public void onMessage(String message) {
		try {
			// TODO SHOWING RECEIVED MSG
			//logger.info(message);
			
			if ( message.indexOf(SUBSCRIPTION_TYPE_MESSAGE) > 0) {
				SubscriptionAck ack = new ObjectMapper().readerFor(SubscriptionAck.class).readValue(message);
				if( ack.response.equals("ok") ) {
					logger.info("Connection established successfully with {} channel {}", WEBSOCKET_URL, ack.channel);
				}else {
					logger.error("Impossible to connect to {}: {}", WEBSOCKET_URL, message);
				}
				
				
			} else if ( message.indexOf(DIFF_ORDER_TYPE_MESSAGE) > 0) {				
				logger.debug(message);
				DiffOrderWrapper wrapper = new ObjectMapper().readerFor(DiffOrderWrapper.class).readValue(message);
				
				if( !SonarService.getInstance().isProcessRealTimeMessages() ) {
					// 2 - Queue any message that come in to this channel: done by onMessage() method
					queue.add(wrapper);
				}else {
					// 6- Apply real-time messages to the local orderbook
					SonarService.getInstance().addDataOrders(wrapper);
				}
			}
		} catch (Exception e) {
			logger.error("Exception parsing message {}. Error: {}", message, e.getMessage());
		}
	}
	

	public ConcurrentLinkedQueue<DiffOrderWrapper> getQueue() {
		return queue;
	}

	@OnWebSocketError
	public void onError(Throwable cause) {
		logger.error("Error received: {}", cause.getMessage());
		close();		
	}

	/**
	 * Channel subscription acknowledge response
	 * 
	 * {"action":"subscribe","response":"ok","time":1528471208600,"type":"diff-orders"}
	 * @author Jorge Marin Guerrero
	 *
	 */
	private static class SubscriptionAck {
		@JsonProperty
		private String action;
		
		@JsonProperty
		private Long time;
		
		@JsonProperty
		private String response;
		
		@JsonProperty("type")
		private String channel;
		
		private SubscriptionAck() {}
	}
}
