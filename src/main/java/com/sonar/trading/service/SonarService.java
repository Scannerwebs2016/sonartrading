package com.sonar.trading.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sonar.trading.model.DiffOrder;
import com.sonar.trading.model.DiffOrder.DiffOrderWrapper;
import com.sonar.trading.model.PositionType;
import com.sonar.trading.model.Strategy;
import com.sonar.trading.model.Trade;
import com.sonar.trading.provider.RestClient;
import com.sonar.trading.provider.WebSocketClient;
import com.sonar.trading.util.PropertiesUtil;

/**
 * Singleton Service class to handling all actions related to orders and trades such as extracting data from rest api calls,
 * web socket subscription in order to be processed (filtered, limited, sorted) and notify the controller once data
 * is ready for being shown.
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class SonarService extends Observable{
	
	private static final Logger logger = LoggerFactory.getLogger(SonarService.class);
	
	protected static final String BOOK_NAME = PropertiesUtil.get("book.name");
	
	private RestClient restClient = null;
	private WebSocketClient webSocketClient = null;
	
	private boolean processRealTimeMessages;
	private BigInteger orderBookSequenceNumber;
	// Max orders of each type (sell/buy) to list
	private Integer maxOrders = Integer.parseInt(PropertiesUtil.get("default.max.orders"));
	
	
	private Strategy strategy;
	
	private List<DiffOrder> buyOrders;
	private List<DiffOrder> sellOrders;
	
	private Set<String> buyOrderIds;
	private Set<String> sellOrderIds;
	
	private Set<DiffOrder> ordersData;
	private List<Trade> tradesData;
		
	
	private static SonarService instance;
	
	public synchronized static SonarService getInstance() {
		if( instance == null ) {
			instance = new SonarService();
		}
		return instance;
	}
	
	private SonarService() {
		processRealTimeMessages = false;
		
		buyOrders = new ArrayList<DiffOrder>();
		sellOrders = new ArrayList<DiffOrder>();
		ordersData = new HashSet<>();
		tradesData = new ArrayList<Trade>();
		
		buyOrderIds = new HashSet<>();
		sellOrderIds = new HashSet<>();
		
		webSocketClient = WebSocketClient.getInstance();		
		restClient = RestClient.getInstance();
		
		strategy = new Strategy(Integer.parseInt(PropertiesUtil.get("default.upticks")), Integer.parseInt(PropertiesUtil.get("default.upticks")));
		
	}
		
	
	public void init() {	
		Executors.newSingleThreadExecutor().submit(() -> {
			// 3- Get full orderbook from REST orderbook end point		
			setOrderBookSequenceNumber(restClient.getOrderBook(BOOK_NAME, Optional.of(false)).getSequence());				
			
			// 4 - Play back the queued message, discarding the ones with sequence number below or equal to orderBookSequenceNumber
			boolean isEmpty = webSocketClient.getQueue().isEmpty();
			while( !isEmpty ) {
				addDataOrders(webSocketClient.getQueue().poll());
				
				// Checking queue after processing orders data to exit or not from the loop: new messages 
				// could have arrived while processing the line above.
				isEmpty = webSocketClient.getQueue().isEmpty();
			}
			// Set the flag to start processing real time messages
			setProcessRealTimeMessages(true);
					
			processOrdersData();
		});	
		
		Executors.newSingleThreadExecutor().submit(() -> {
			// 5 - Poll the most recent trades
			processTradesData();
		});		
		
	}
	
	public void addDataOrders(DiffOrderWrapper wrapper) {
		if( wrapper.getSequence().compareTo(orderBookSequenceNumber) > 0) {
			wrapper.getDiffOrders().stream().forEach(o->{
				if( o.getValue() != null) {
					ordersData.add(o);
				}
			});
			
			if( processRealTimeMessages && !ordersData.isEmpty() ) {
				processOrdersData();
			}
		}
	}
	
	/**
	 * Method to process trade data requested using the rest client to be shown in the chart once the observer 
	 * is notified
	 */
	public void processTradesData() {
		tradesData.clear();
		
		tradesData.addAll(restClient.getTrades(BOOK_NAME, Optional.empty(), Optional.empty(), Optional.empty()).stream().limit(maxOrders).collect(Collectors.toList()));
		
		// Applying strategy
		strategy.evaluate(tradesData);
		
		// Notify
		setChanged();
		notifyObservers(tradesData.toArray(new Trade[tradesData.size()]));				
	}
	
	/**
	 * Method to process order data: grouping (buys/sells), sorting (by rate) and limiting the results to be shown 
	 * in the chart once the observer is notified
	 */
	private void processOrdersData() {
		Map<PositionType, List<DiffOrder>> grouped = ordersData.stream()
				.collect(Collectors.groupingBy(DiffOrder::getType, Collectors.toList()));
		
		Set<String> auxBuyOrderIds = new HashSet<>();
		Set<String> auxSellOrderIds = new HashSet<>();
		
		grouped.keySet().stream().forEach(
				key->{
					List<DiffOrder> orders = grouped.get(key);
					if( key.equals(PositionType.BUY) ) {
						buyOrders.clear();
						buyOrders = orders.stream()
							.sorted(Comparator.comparing(DiffOrder::getRate).reversed())
							.limit(maxOrders).collect(Collectors.toList());
						buyOrders.forEach(o -> auxBuyOrderIds.add(o.getId()));
					}else {
						sellOrders.clear();
						sellOrders = orders.stream()
							.sorted(Comparator.comparing(DiffOrder::getRate).reversed())
							.limit(maxOrders).collect(Collectors.toList());
						
						sellOrders.forEach(o -> auxSellOrderIds.add(o.getId()));						
					}					
				});			
		
		boolean refresh = false;
		if( sellOrderIds.isEmpty() || buyOrderIds.isEmpty()) {
			refresh = true;
		}else if( !sellOrderIds.containsAll(auxSellOrderIds) || !buyOrderIds.containsAll(auxBuyOrderIds)) {
			refresh = true;			
		}
		
		if( refresh ) {
			sellOrderIds = new HashSet<>(auxSellOrderIds);
			buyOrderIds = new HashSet<>(auxBuyOrderIds);
			
			List<DiffOrder> filtered = new ArrayList<>();
			filtered.addAll(buyOrders);
			filtered.addAll(sellOrders);
			
			// Notify
			setChanged();
			notifyObservers(filtered.toArray(new DiffOrder[filtered.size()]));	
		}				
	}
	
	

	public void close() {
		if( restClient != null ) {
			logger.info("Closing rest client ...");
			restClient.close();
		}
		if( webSocketClient != null ) {
			logger.info("Closing web socket client ...");
			webSocketClient.close();
		}		
	}
	
	// Getters & Setters
	public boolean isProcessRealTimeMessages() {
		return processRealTimeMessages;
	}
	
	public void setProcessRealTimeMessages(boolean processRealTimeMessages) {
		this.processRealTimeMessages = processRealTimeMessages;
	}
	
	public Integer getMaxOrders() {
		return maxOrders;
	}
	
	public void setMaxOrders(Integer maxOrders) {
		this.maxOrders = maxOrders;
	}
	
	public void setOrderBookSequenceNumber(BigInteger sequenceNumber) {
		orderBookSequenceNumber = sequenceNumber;
	}

	public int getUpTicksCount() {
		return strategy.getUpTicksCount();
	}

	public void setUpTicksCount(int upTicksCount) {
		this.strategy.setUpTicksCount(upTicksCount);
	}

	public int getDownTicksCount() {
		return strategy.getDownTicksCount();
	}

	public void setDownTicksCount(int downTicksCount) {
		this.strategy.setDownTicksCount(downTicksCount);
	}

	
	
}
