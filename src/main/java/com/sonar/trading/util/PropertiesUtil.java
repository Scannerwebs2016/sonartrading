package com.sonar.trading.util;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class PropertiesUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(PropertiesUtil.class);
	
	private static final String CONFIG_FILE = "config.properties";
	private static Properties properties;
	
	static {
		try {
			properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(CONFIG_FILE));
			
			logger.info("Configuration file {} loaded successfully", CONFIG_FILE);
		} catch (Exception e) {
			String msg = "Impossible to load configuration file: " + e.getMessage();
			logger.error(msg);
			throw new RuntimeException(msg);
		}
	}
	
	
	public static String get(String name) {
		String value = properties.getProperty(name);
		if( null == value) {
			String msg = "Property " + name + " not found";
			logger.error(msg);
			throw new RuntimeException(msg);
		}
		return value;
	}

}
