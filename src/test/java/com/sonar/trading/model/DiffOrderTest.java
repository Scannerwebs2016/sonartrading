package com.sonar.trading.model;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sonar.trading.model.DiffOrder.DiffOrderWrapper;


/**
 * Test class for {@link com.sonar.trading.model.DiffOrder}
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class DiffOrderTest {

	private final String JSON = "{"
			+ "\"type\":\"diff-orders\","
			+ "\"book\":\"btc_mxn\","
			+ "\"payload\":["
			+ "{"
			+ "\"o\":\"feASoZJIKnZaVu2P\","
			+ "\"d\":1528468965717,"
			+ "\"r\":\"152665.66\","
			+ "\"t\":0,"
			+ "\"a\":\"0.05266399\","
			+ "\"v\":\"8039.98279158\","
			+ "\"s\":\"open\""
			+ "}"
			+ "],"
			+ "\"sequence\":94416412}";
	
	@Test
	public void testParse() throws IOException {
		DiffOrderWrapper wrapper = new ObjectMapper().readerFor(DiffOrderWrapper.class).readValue(JSON);
		
		assertEquals("diff-orders", wrapper.getType());
		assertEquals("btc_mxn", wrapper.getBook());
		assertEquals(new BigInteger("94416412"), wrapper.getSequence());
		
		List<DiffOrder> diffOrders = wrapper.getDiffOrders();
		assertEquals(1, diffOrders.size());
		
		DiffOrder diffOrder = diffOrders.get(0);
		assertEquals(String.format("%.8f", new BigDecimal(0.05266399)), diffOrder.getAmount().toPlainString());
		assertEquals("feASoZJIKnZaVu2P", diffOrder.getId());
		assertEquals(String.format("%.2f", new BigDecimal(152665.66)), diffOrder.getRate().toPlainString());
		assertEquals(PositionType.BUY, diffOrder.getType());
		assertEquals(StatusType.OPEN, diffOrder.getStatus());
		assertEquals("2018-06-08T14:42:45.717Z[UTC]", diffOrder.getTimestamp().toString());
		assertEquals(String.format("%.2f", new BigDecimal(8039.98)), String.format("%.2f", diffOrder.getValue()));
	}

}
