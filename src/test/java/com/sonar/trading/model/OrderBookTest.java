package com.sonar.trading.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sonar.trading.model.OrderBook.OrderBookWrapper;

/**
 * Test class for {@link com.sonar.trading.model.OrderBook}
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class OrderBookTest {

	private final String JSON = "{\"success\":true,"
			+ "\"payload\":{"
			+ "\"updated_at\":\"2018-06-05T17:28:52+00:00\","
			+ "\"bids\":["
			+ "{\"book\":\"btc_mxn\",\"price\":\"154300.86\",\"amount\":\"0.36725994\",\"oid\":\"7Cyo2siXsS088elk\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"145000.00\",\"amount\":\"0.00031725\",\"oid\":\"pILX616keZqQOpNc\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"145000.00\",\"amount\":\"0.00689656\",\"oid\":\"h9RwbRWYHmEsweuP\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"145000.00\",\"amount\":\"0.00012370\",\"oid\":\"nG9fZDuE06wTX5ov\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"145000.00\",\"amount\":\"0.00100000\",\"oid\":\"cJdatMfGuvFyX15q\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"145000.00\",\"amount\":\"0.03473794\",\"oid\":\"6zG478bpYf37ULuk\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"500.00\",\"amount\":\"10.06454001\",\"oid\":\"Stac5w4NyLxeugVb\"}],"
			+ "\"asks\":["
			+ "{\"book\":\"btc_mxn\",\"price\":\"154425.00\",\"amount\":\"0.00099985\",\"oid\":\"j25uqgJAQPxZ7iMW\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"154466.67\",\"amount\":\"0.00199985\",\"oid\":\"pdzz7KHgz8kbuDnI\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"16000000.00\",\"amount\":\"0.00216811\",\"oid\":\"sv3Tg15y3nQdaxou\"}],"
			+ "\"sequence\":\"93842319\"}}";

	@Test
	public void testParse() throws IOException {
		OrderBookWrapper wrapper = new ObjectMapper().readerFor(OrderBookWrapper.class).readValue(JSON);
		
		assertTrue(wrapper.getSuccess());
		
		OrderBook orderBook = wrapper.getOrderBook();		
		
		assertEquals("2018-06-05T17:28:52Z", orderBook.getUpdatedAt().toString());
		assertEquals(7, orderBook.getBids().size());
		assertEquals(3, orderBook.getAsks().size());
		
		Order order = orderBook.getBids().get(0);
		assertEquals("btc_mxn", order.getBook());
		assertEquals(String.format("%.2f", new BigDecimal(154300.86)), order.getPrice().toPlainString());
		assertEquals(String.format("%.8f", new BigDecimal(0.36725994)), order.getAmount().toPlainString());
		assertEquals("7Cyo2siXsS088elk", order.getOid());
		
		order = orderBook.getAsks().get(0);
		assertEquals("btc_mxn", order.getBook());
		assertEquals(String.format("%.2f", new BigDecimal(154425.00)), order.getPrice().toPlainString());
		assertEquals(String.format("%.8f", new BigDecimal(0.00099985)), order.getAmount().toPlainString());
		assertEquals("j25uqgJAQPxZ7iMW", order.getOid());		
		
	}


}
