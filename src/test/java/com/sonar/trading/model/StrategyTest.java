package com.sonar.trading.model;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * Test class for {@link com.sonar.trading.model.Strategy}
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class StrategyTest {

	private int upTicksCount;
	private int downTicksCount;
	
	@Test
	public void testEvaluate_1_1() {
		upTicksCount = 1;
		downTicksCount = 1;
		
		List<Trade> trades = populate();
		new Strategy(upTicksCount, downTicksCount).evaluate(trades);
		
		for(int index = 0; index < trades.size(); index++) {
			assertEquals(result(upTicksCount,downTicksCount)[index], trades.get(index).getStrategyPosition());
		}		
	}
	
	@Test
	public void testEvaluate_2_3() {
		upTicksCount = 2;
		downTicksCount = 3;
		
		List<Trade> trades = populate();
		new Strategy(upTicksCount, downTicksCount).evaluate(trades);
		
		for(int index = 0; index < trades.size(); index++) {
			assertEquals(result(upTicksCount,downTicksCount)[index], trades.get(index).getStrategyPosition());
		}		
	}
	
	private List<Trade> populate() {
		List<Trade> trades = new ArrayList<>();
		
								//upTicksCount/downTicksCount			1/1		2/3
		
		trades.add(new Trade(new BigDecimal(2000.00)));	//			=> 	-		-
		trades.add(new Trade(new BigDecimal(2000.00))); // ZERO		=>	-		-
		trades.add(new Trade(new BigDecimal(1000.00)));	// DOWN		=>	BUY		-
		trades.add(new Trade(new BigDecimal(3000.00)));	// UP		=>	SELL	-
		trades.add(new Trade(new BigDecimal(4000.00)));	// UP		=>	SELL	SELL
		trades.add(new Trade(new BigDecimal(2000.00)));	// DOWN		=>	BUY		-
		trades.add(new Trade(new BigDecimal(4000.00)));	// UP		=>	SELL	-
		trades.add(new Trade(new BigDecimal(3000.00)));	// DOWN		=>	BUY		-
		trades.add(new Trade(new BigDecimal(3000.00)));	// ZERO		=>	-		-
		trades.add(new Trade(new BigDecimal(2000.00)));	// DOWN		=>	BUY		-
		trades.add(new Trade(new BigDecimal(1000.00)));	// DOWN		=>	BUY		-
		trades.add(new Trade(new BigDecimal(999.00)));	// DOWN		=>	BUY		BUY
		trades.add(new Trade(new BigDecimal(2000.00)));	// UP		=>	SELL	-
		
		return trades;
	}
	
	private PositionType[] result(int upTicksCount, int downTicksCount) {
		PositionType[] result = null;
		if( upTicksCount == 1 && downTicksCount == 1) {
			result = new PositionType[] {
					null,
					null, 
					PositionType.BUY, 
					PositionType.SELL, 
					PositionType.SELL, 
					PositionType.BUY, 
					PositionType.SELL, 
					PositionType.BUY, 
					null, 
					PositionType.BUY, 
					PositionType.BUY, 
					PositionType.BUY,
					PositionType.SELL};
		}
		
		if( upTicksCount == 2 && downTicksCount == 3) {
			result = new PositionType[] {
					null,
					null, 
					null, 
					null, 
					PositionType.SELL, 
					null, 
					null, 
					null, 
					null, 
					null, 
					null,
					PositionType.BUY, 
					null};
		}
		return result;
	}
}
