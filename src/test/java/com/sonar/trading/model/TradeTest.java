package com.sonar.trading.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sonar.trading.model.Trade.TradeWrapper;

/**
 * Test class for {@link com.sonar.trading.model.Trade}
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class TradeTest {

	private final String JSON = "{" + 
			"    \"success\": true," + 
			"    \"payload\": [" + 
			"        {" + 
			"            \"book\": \"btc_mxn\"," + 
			"            \"created_at\": \"2018-06-07T18:50:13+0000\"," + 
			"            \"amount\": \"0.00105056\"," + 
			"            \"maker_side\": \"buy\"," + 
			"            \"price\": \"154790.61\"," + 
			"            \"tid\": 7730008" + 
			"        }," + 
			"        {" + 
			"            \"book\": \"btc_mxn\"," + 
			"            \"created_at\": \"2018-06-07T18:50:18+0000\"," + 
			"            \"amount\": \"0.00099991\"," + 
			"            \"maker_side\": \"sell\"," + 
			"            \"price\": \"154790.99\"," + 
			"            \"tid\": 7730007" + 
			"        }" + 
			"    ]" + 
			"}";

	@Test
	public void testParse() throws IOException {
		TradeWrapper wrapper = new ObjectMapper().readerFor(TradeWrapper.class).readValue(JSON);
		
		assertTrue(wrapper.getSuccess());
		
		List<Trade> trades = wrapper.getTrades();
		assertEquals(2, trades.size());
				
		Trade trade = trades.get(0);
		
		assertEquals(String.format("%.8f", new BigDecimal(0.00105056)), trade.getAmount().toPlainString());
		assertEquals("btc_mxn", trade.getBook());
		assertEquals(PositionType.BUY, trade.getMakerSide());
		assertEquals(String.format("%.2f", new BigDecimal(154790.61)), trade.getPrice().toPlainString());
		assertEquals("2018-06-07T18:50:13Z", trade.getCreatedAt().toString());
		assertEquals(7730008L, trade.getTid().longValue());
	}


}
