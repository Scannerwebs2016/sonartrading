package com.sonar.trading.provider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sonar.trading.model.Order;
import com.sonar.trading.model.OrderBook;
import com.sonar.trading.model.Trade;
import com.sonar.trading.model.PositionType;
import com.sonar.trading.util.PropertiesUtil;
import static org.mockito.Mockito.*;

/**
 * Test class for {@link com.sonar.trading.provider.RestClient}
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class RestClientTest {

	private final String ORDER_BOOK_JSON_SUCCESS = "{\"success\":true,"
			+ "\"payload\":{"
			+ "\"updated_at\":\"2018-06-05T17:28:52+00:00\","
			+ "\"bids\":["
			+ "{\"book\":\"btc_mxn\",\"price\":\"154300.86\",\"amount\":\"0.36725994\",\"oid\":\"7Cyo2siXsS088elk\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"145000.00\",\"amount\":\"0.00031725\",\"oid\":\"pILX616keZqQOpNc\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"145000.00\",\"amount\":\"0.00689656\",\"oid\":\"h9RwbRWYHmEsweuP\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"145000.00\",\"amount\":\"0.00012370\",\"oid\":\"nG9fZDuE06wTX5ov\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"145000.00\",\"amount\":\"0.00100000\",\"oid\":\"cJdatMfGuvFyX15q\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"145000.00\",\"amount\":\"0.03473794\",\"oid\":\"6zG478bpYf37ULuk\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"500.00\",\"amount\":\"10.06454001\",\"oid\":\"Stac5w4NyLxeugVb\"}],"
			+ "\"asks\":["
			+ "{\"book\":\"btc_mxn\",\"price\":\"154425.00\",\"amount\":\"0.00099985\",\"oid\":\"j25uqgJAQPxZ7iMW\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"154466.67\",\"amount\":\"0.00199985\",\"oid\":\"pdzz7KHgz8kbuDnI\"},"
			+ "{\"book\":\"btc_mxn\",\"price\":\"16000000.00\",\"amount\":\"0.00216811\",\"oid\":\"sv3Tg15y3nQdaxou\"}],"
			+ "\"sequence\":\"93842319\"}}";

	private final String ORDER_BOOK_JSON_ERROR = "{\"success\":false,"
			+ "\"payload\":{}}";

	private final String TRADES_JSON_SUCCESS = "{" + 
			"    \"success\": true," + 
			"    \"payload\": [" + 
			"        {" + 
			"            \"book\": \"btc_mxn\"," + 
			"            \"created_at\": \"2018-06-07T18:50:13+0000\"," + 
			"            \"amount\": \"0.00105056\"," + 
			"            \"maker_side\": \"buy\"," + 
			"            \"price\": \"154790.61\"," + 
			"            \"tid\": 7730008" + 
			"        }," + 
			"        {" + 
			"            \"book\": \"btc_mxn\"," + 
			"            \"created_at\": \"2018-06-07T18:50:18+0000\"," + 
			"            \"amount\": \"0.00099991\"," + 
			"            \"maker_side\": \"sell\"," + 
			"            \"price\": \"154790.99\"," + 
			"            \"tid\": 7730007" + 
			"        }" + 
			"    ]" + 
			"}";
	
	private final String TRADES_JSON_ERROR = "{" + 
			"    \"success\": false," + 
			"    \"payload\": []" + 
			"}";


	
	private RestClient client;
	private ResteasyClient mockHttpClient;
	private final String BOOK = PropertiesUtil.get("book.name");
	
	@Before
	public void setUp() {
		mockHttpClient = mock(ResteasyClient.class);
		
		client = RestClient.getInstance();
		assertNotNull(client);
		
		RestClient.httpClient = mockHttpClient;	
	}
	
	@After
	public void tearDown() {
		client.close();
	}
	
	@Test
	public void testGetOrderBook() throws URISyntaxException {		
		mockRestApiResponse(RestClient.ENDPOINT_ORDERBOOK, ORDER_BOOK_JSON_SUCCESS);
		
		OrderBook orderBook = client.getOrderBook(BOOK, Optional.of(false));		
		assertNotNull(orderBook);
		
		assertEquals("2018-06-05T17:28:52Z", orderBook.getUpdatedAt().toString());
		assertEquals(7, orderBook.getBids().size());
		assertEquals(3, orderBook.getAsks().size());
		
		Order order = orderBook.getBids().get(0);
		assertEquals("btc_mxn", order.getBook());
		assertEquals(String.format("%.2f", new BigDecimal(154300.86)), order.getPrice().toPlainString());
		assertEquals(String.format("%.8f", new BigDecimal(0.36725994)), order.getAmount().toPlainString());
		assertEquals("7Cyo2siXsS088elk", order.getOid());
		
		order = orderBook.getAsks().get(0);
		assertEquals("btc_mxn", order.getBook());
		assertEquals(String.format("%.2f", new BigDecimal(154425.00)), order.getPrice().toPlainString());
		assertEquals(String.format("%.8f", new BigDecimal(0.00099985)), order.getAmount().toPlainString());
		assertEquals("j25uqgJAQPxZ7iMW", order.getOid());		
	}
	
	@Test(expected=RuntimeException.class)
	public void testGetOrderBook_error() throws URISyntaxException {		
		mockRestApiResponse(RestClient.ENDPOINT_ORDERBOOK, ORDER_BOOK_JSON_ERROR);
		
		client.getOrderBook(BOOK, Optional.of(false));		
	}
	
	@Test
	public void testGetTrades() throws URISyntaxException {	
		mockRestApiResponse(RestClient.ENDPOINT_TRADES, TRADES_JSON_SUCCESS);
		
		List<Trade> trades = client.getTrades(BOOK, Optional.empty(), Optional.empty(), Optional.empty());		
		assertNotNull(trades);
		
		assertEquals(2, trades.size());
		
		Trade trade = trades.get(0);
		
		assertEquals(String.format("%.8f", new BigDecimal(0.00105056)), trade.getAmount().toPlainString());
		assertEquals("btc_mxn", trade.getBook());
		assertEquals(PositionType.BUY, trade.getMakerSide());
		assertEquals(String.format("%.2f", new BigDecimal(154790.61)), trade.getPrice().toPlainString());
		assertEquals("2018-06-07T18:50:13Z", trade.getCreatedAt().toString());
		assertEquals(7730008L, trade.getTid().longValue());
	}
	
	@Test(expected=RuntimeException.class)
	public void testGetTrades_error() throws URISyntaxException {
		mockRestApiResponse(RestClient.ENDPOINT_TRADES, TRADES_JSON_ERROR);
		client.getTrades(BOOK, Optional.empty(), Optional.empty(), Optional.empty());
	}
	
	
	private void mockRestApiResponse(String endPoint, String json) throws URISyntaxException {
		ResteasyWebTarget mockTarget = mock(ResteasyWebTarget.class);
		Response mockRresponse = mock(Response.class);
		Builder mockBuilder = mock(Builder.class);
				
		when(mockTarget.queryParam("aggregate", false)).thenReturn(mockTarget);
		when(mockTarget.getUri()).thenReturn(new URI(""));
		when(mockHttpClient.target(RestClient.BASE_URL)).thenReturn(mockTarget);
		when(mockTarget.path(endPoint)).thenReturn(mockTarget);
		when(mockTarget.queryParam("book", BOOK)).thenReturn(mockTarget);
		
		when(mockTarget.request()).thenReturn(mockBuilder);
		when(mockBuilder.get()).thenReturn(mockRresponse);
		when(mockRresponse.readEntity(String.class)).thenReturn(json);
	}
}
