package com.sonar.trading.provider;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * Test class for {@link com.sonar.trading.provider.WebSocketClient}
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class WebsocketClientTest {
	
	private WebSocketClient client;
	
	private String JSON_MSG_SUBSCRIPTION = "{\"action\":\"subscribe\",\"response\":\"ok\",\"time\":1530645721416,\"type\":\"diff-orders\"}";

	private String JSON_MSG_DIFFORDER ="{\"type\":\"diff-orders\",\"book\":\"btc_mxn\",\"payload\":[{\"o\":\"djwWMgP54Lq0AkF8\",\"d\":1530645722639,\"r\":\"10000\",\"t\":0,\"a\":\"0.01\",\"v\":\"100\",\"s\":\"open\"}],\"sequence\":100084712}";
	private String JSON_MSG_INVALID_DIFFORDER ="{\"type\":\"others\"}";
	
	
	@Before
	public void setUp() {
		client = WebSocketClient.getInstance();
	}
	
	@After
	public void tearDown() {
		client.close();
	}
	
	@Test
	public void testOnMessage_1_message() {
		sendMessages(1);		
		assertEquals(1, client.getQueue().size());
	}
	
	@Test
	public void testOnMessage_3_message() {
		sendMessages(3);		
		assertEquals(3, client.getQueue().size());
	}

	private void sendMessages(int messages) {
		client.onMessage(JSON_MSG_SUBSCRIPTION);
		for(int i=0; i<messages; i++) {
			client.onMessage(JSON_MSG_DIFFORDER);
		}
		client.onMessage(JSON_MSG_INVALID_DIFFORDER);
	}
}
