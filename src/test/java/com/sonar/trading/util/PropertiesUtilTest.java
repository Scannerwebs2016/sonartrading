package com.sonar.trading.util;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

/**
 * Test class for {@link com.sonar.trading.util.PropertiesUtil}
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class PropertiesUtilTest {

	@Test
	public void testGet() throws IOException {
		assertEquals("wss://ws.bitso.com", 			PropertiesUtil.get("websocket.url"));
		assertEquals("https://api.bitso.com/v3/", 	PropertiesUtil.get("api.base.url"));
		assertEquals("trades/", 					PropertiesUtil.get("api.endpoint.trades"));
		assertEquals("order_book/", 				PropertiesUtil.get("api.endpoint.orderbook"));
		assertEquals("btc_mxn", 					PropertiesUtil.get("book.name"));
		assertEquals("20",		 					PropertiesUtil.get("default.max.orders"));
		assertEquals("250",			 				PropertiesUtil.get("default.upper.limit"));
		assertEquals("1", 							PropertiesUtil.get("default.upticks"));
		assertEquals("1", 							PropertiesUtil.get("default.downticks"));
	}


}
